##### Vim
* Sort stuff in `vim/common.vim` and add folds.
* Use [par](http://vimcasts.org/episodes/formatting-text-with-par/) for automatic
  paragraph formatting?
* Try these Vim plugins:
  [Vimwiki](https://github.com/vimwiki/vimwiki),
  [vim-markdown-folding](https://github.com/nelstrom/vim-markdown-folding),
  [ack.vim](https://github.com/mileszs/ack.vim),
  [ag.vim](https://github.com/rking/ag.vim),
  [vim-quicklink](https://github.com/christoomey/vim-quicklink),
  [Projectionist](https://github.com/tpope/vim-projectionist),
  [Powerline](https://github.com/powerline/powerline),
  [vim-pasta](https://github.com/sickill/vim-pasta).

<!-- ##### Mutt -->

<!-- vim: set tw=90 sts=-1 sw=4 et: -->
